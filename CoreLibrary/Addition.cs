﻿using System;

namespace CoreLibrary
{
    public class Addition
    {
        public int Add(int x, int y)
        {
            return x + y;
        }
    }
}
